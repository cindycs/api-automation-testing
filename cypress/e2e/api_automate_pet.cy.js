// const dataJson = require('../../fixtures/pet.json')

describe ('API automation', function ()
{
    it('GET-list pet by id', function (){
        cy.request({
            method : 'GET',
            url : 'https://petstore.swagger.io/v2/pet/16',
            headers : {
                "accept" : "application/json"
            }
        }).then (function (response){
            expect(response.status).equal(200);
            expect(response.body.id).equal(16);
            expect(response.body.category.id).equal(26);
            expect(response.body.category.name).equal('doggie-6 updated');
        })
    })

    it('POST-Add a new pet to store', function (){
        cy.fixture('createpet').then((payload) => {
        cy.request({
            method : 'POST',
            url : 'https://petstore.swagger.io/v2/pet',
            headers : {
                "accept" : "application/json",
                "Content-Type" : "application/json"
            },
            body : {
                "id": payload.id,
                    "category": {
                    "id": payload.category.id,
                    "name": payload.category.name
                }   
            }
        }).then (function (response){
            expect(response.status).equal(200);
            expect(response.body.id).to.equal(payload.id);
            expect(response.body.category.id).to.equal(payload.category.id);
            expect(response.body.category.name).to.equal(payload.category.name);
        })      
      })
    })

    it('POST-Add a new pet to store with invalid data - Negative Case', function (){
        cy.fixture('createnegativepet').then((payload) => {
        cy.request({
            method : 'POST',
            url : 'https://petstore.swagger.io/v2/pet',
            headers : {
                "accept" : "application/json",
                "Content-Type" : "application/json"
            },
            body : {
                "id": payload.id,
                    "category": {
                    "id": payload.category.id,
                    "name": payload.category.name
                }   
            }
        }).then (function (response){
            expect(response.status).equal(200);
            expect(response.body.id).to.equal(payload.id);
            expect(response.body.category.id).to.equal(payload.category.id);
            expect(response.body.category.name).to.equal(payload.category.name);
        })      
      })
    })

    it('PUT-Update pet', function (){
        cy.fixture('updatepet').then((payload) => {
        cy.request({
            method : 'PUT',
            url : 'https://petstore.swagger.io/v2/pet',
            headers : {
                "accept" : "application/json",
                "Content-Type" : "application/json"
            },
            body : {
                "id": payload.id,
                    "category": {
                    "id": payload.category.id,
                    "name": payload.category.name
                }   
            }
        }).then (function (response){
            expect(response.status).equal(200);
            expect(response.body.id).to.equal(payload.id);
            expect(response.body.category.id).to.equal(payload.category.id);
            expect(response.body.category.name).to.equal(payload.category.name);
      })
     })
    })

     it('DELETE-Delete pet', function (){
        cy.request({
            method : 'DELETE',
            url : 'https://petstore.swagger.io/v2/pet/15',
            headers : {
                "accept" : "application/json"
            },
        }).then (function (response){
            expect(response.status).equal(200);
        })
       })
})
